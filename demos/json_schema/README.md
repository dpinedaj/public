[![N|Solid](https://miro.medium.com/max/700/1*uSoiPBhcwIwEFDYyv28SvQ.png)](https://www.linkedin.com/in/luisvasv/)
# **JSON SCHEMA**
****
JSON Schema is a vocabulary that allows you to annotate and validate JSON documents.

**Advantages**

- Describes your existing data format(s).
- Provides clear human- and machine- readable documentation.
- Validates data which is useful for:
- Automated testing.
- Ensuring quality of client submitted data.

## **prerequisites**
****
- Have python installed
- Have installed MongoDB(*Optional if you want work only with python*)

## **Instructions**
****
1. Install dependencies required
 
    conda commands:
    ```sh
    conda env create -f demos/json_schema/environment/conda-environment.yml 
    conda activate demo_js
    ```
    Pip commands:
    ```sh
    pip install -r demos/json_schema/environment/requirements.txt
    ```
2. Run code
    
    If you want to validate JSON schema in Python, please enable or disable the comments of file app.py 
    and then execute : 

    ```sh
    python app.py
    ```
    
    If you want to validate JSON schema in MongoDB, please execute:
     ```sh
    bash app.sh
    ```
## **References**
****

* [pypi-jsonschema](https://pypi.org/project/jsonschema/) - An implementation of JSON Schema validation for Python
* [json-schema-validation](http://json-schema.org/draft/2019-09/json-schema-validation.html) - A Vocabulary for Structural Validation of JSON
* [json-schema.org](https://json-schema.org/) - Official JSON Schema Page
* [jq](https://stedolan.github.io/jq/download/) - is like sed for JSON data
